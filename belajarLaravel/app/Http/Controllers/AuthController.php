<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('register');
    }
    public function masuk(Request $request)
    {
        // dd($request->all());
        $awal = $request['first'];
        $akhir = $request['last'];

        return view('wellcome', ['first'=> $awal, 'last'=> $akhir]);
    }
}
