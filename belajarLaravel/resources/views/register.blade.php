@extends('layout.master')
@section('title')
    Pendaftaran
@endsection
@section('sub-title')
    Daftar
@endsection
@section('content')
    
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
<form action="/wellcome" method="post">
    @csrf
    <label>First name:</label><br><br>
    <input type="text" name="first"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="last"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="Gender">Male <br>
    <input type="radio" name="Gender">Female <br>
    <input type="radio" name="Gender">Other <br><br>
    <label>Nationality:</label><br><br>
    <select name="Nationality">
        <option value="">Indonesian</option>
        <option value="">United Kingdom</option>
        <option value="">Arabic</option>
        <option value="">China</option>
    </select><br><br>
    <label for="Languange Spoken">Languange Spoken:</label><br><br>
    <input type="checkbox" name="Languange Spoken">Bahasa Indonesia <br>
    <input type="checkbox" name="Languange Spoken">English <br>
    <input type="checkbox" name="Languange Spoken">Other <br><br>
    <label>Bio:</label><br><br>
    <textarea cols="30" rows="10"></textarea><br>

    <input type="submit" value="Sign Up">
</form>
@endsection