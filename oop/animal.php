<?php
class Animal
{
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

    public function __construct($nama)
    {
        $this->name = $nama;
    }
}
// $sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 4
// echo $sheep->cold_blooded; // "no"
